
CREATE TABLE products(
    product_id SERIAL4 PRIMARY KEY ,
    product_name VARCHAR(250) NOT NULL ,
    product_price NUMERIC NOT NULL
);


CREATE TABLE products(
     customer_id SERIAL4 PRIMARY KEY ,
     customer_name VARCHAR(250) NOT NULL ,
     customer_address VARCHAR(100) NOT NULL ,
     customer_phone VARCHAR(20) NOT NULL
);


CREATE TABLE invoices(
  invoice_id SERIAL4 PRIMARY KEY ,
  invoice_date TIMESTAMP,
  customer_id INT REFERENCES customers(customer_id)
);

CREATE TABLE invoice_detail(
    invoice_id INT REFERENCES invoices(invoice_id) ON DELETE CASCADE ON UPDATE CASCADE ,
    product_id INT REFERENCES products(product_id) ON DELETE CASCADE ON UPDATE CASCADE ,
    PRIMARY KEY (invoice_id, product_id)
);