package com.example.spring_homework02.controller;

import com.example.spring_homework02.model.entity.Invoice;
import com.example.spring_homework02.model.request.InvoiceRequest;
import com.example.spring_homework02.model.response.InvoiceResponse;
import com.example.spring_homework02.service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/invoices")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/all")

    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){

        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .message("Fetch successful")
                .payload(invoiceService.getAllInvoices())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{id}")
    public  ResponseEntity<InvoiceResponse<Invoice>>  getInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                .message("Fetch by ID successful")
                .payload(invoiceService.getInvoiceById(invoiceId))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @PostMapping("/")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        InvoiceResponse<Invoice> response = null;

        Integer storeInvoiceId = invoiceService.addNewInvioce(invoiceRequest);

        if(storeInvoiceId != null){
            response = InvoiceResponse.<Invoice>builder()
                    .message("Add Invoice Success")
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
