package com.example.spring_homework02.controller;

import com.example.spring_homework02.model.entity.Customer;
import com.example.spring_homework02.model.entity.Product;
import com.example.spring_homework02.model.request.CustomerRequest;
import com.example.spring_homework02.model.request.ProductRequest;
import com.example.spring_homework02.model.response.CustomerRespone;
import com.example.spring_homework02.model.response.ProductResponse;
import com.example.spring_homework02.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")

    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){

           ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                   .message("Fetch successful")
                   .payload(productService.getAllProducts())
                   .httpStatus(HttpStatus.OK)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<ProductResponse<Product>>  getProductById(@PathVariable("id") Integer productId){
        ProductResponse<Product> response = ProductResponse.<Product>builder()
                .message("Fetch by ID successful")
                .payload(productService.getProductById(productId))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response = null;
        if (productService.deleteProductById(productId)==true){
            response = ProductResponse.<String>builder()
                    .message("Delete By Id successful")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);
    }


    @PostMapping
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){

        ProductResponse<Product> response=null;
        Integer storeProductId = productService.addNewProduct(productRequest);

        if(productService.getProductById(storeProductId) != null){
            response = ProductResponse.<Product>builder()
                    .message("Add Product success")
                    .payload(productService.getProductById(storeProductId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponse<Product>> updateProduct(
            @RequestBody ProductRequest productRequest, @PathVariable("id") Integer productId
    ){
        ProductResponse<Product> response = null;

        if (productService.getProductById(productId) != null){
            Integer productIdStoreUpdate = productService.updateProduct(productRequest,productId);
            response = ProductResponse.<Product>builder()
                    .message("Update success")
                    .payload(productService.getProductById(productIdStoreUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
