package com.example.spring_homework02.controller;
import com.example.spring_homework02.model.entity.Customer;
import com.example.spring_homework02.model.request.CustomerRequest;
import com.example.spring_homework02.model.response.CustomerRespone;
import com.example.spring_homework02.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/customers")
public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")

    public ResponseEntity<CustomerRespone<List<Customer>>> getAllCustomer(){

        CustomerRespone<List<Customer>> response = CustomerRespone.<List<Customer>>builder()
                .message("Fetch successful")
                .payload(customerService.getAllCustomers())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<CustomerRespone<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
       CustomerRespone<Customer> response = CustomerRespone.<Customer>builder()
                .message("Fetch by ID successful")
                .payload(customerService.getCustomerById(customerId))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CustomerRespone<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerRespone<String> response = null;
        if (customerService.deleteCustomerById(customerId)==true){
            response = CustomerRespone.<String>builder()
                    .message("Delete By Id successful")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);
    }


    @PostMapping
    public ResponseEntity<CustomerRespone<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){

        CustomerRespone<Customer> response= null;
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);

        if(customerService.getCustomerById(storeCustomerId) != null){
            response = CustomerRespone.<Customer>builder()
                    .message("Add Customer success")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerRespone<Customer>> updateCustomer(
            @RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerId
    ){
        CustomerRespone<Customer> response = null;

        if (customerService.getCustomerById(customerId) != null){
            Integer customerIdStoreUpdate = customerService.updateCustomer(customerRequest,customerId);
            response = CustomerRespone.<Customer>builder()
                    .message("Update success")
                    .payload(customerService.getCustomerById(customerIdStoreUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
