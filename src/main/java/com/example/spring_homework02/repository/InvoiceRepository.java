package com.example.spring_homework02.repository;

import com.example.spring_homework02.model.entity.Invoice;
import com.example.spring_homework02.model.entity.Product;
import com.example.spring_homework02.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoices")
    @Results(
            id = "invoiceMapper",
            value = {
                    @Result(property = "invoice_id",column = "invoice_id"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select = "com.example.spring_homework02.repository.CustomerRepository.getCustomerById")
                    ),
                    @Result(property = "products", column = "invoice_id",
                            many = @Many(select = "com.example.spring_homework02.repository.ProductRepository.getProductByInvoiceId")
                    )
            }
    )
    List<Invoice> findAllInvoice();

    @Select("SELECT * FROM invoices WHERE invoice_id=#{invoiceId}")
    @ResultMap("invoiceMapper")
    Invoice findById(Integer invoiceId);

    @Select( " INSERT INTO invoices ( invoice_date, customer_id) " +
            " VALUES ( #{request.invoice_date}, #{request.customer_id})" +
    "RETURNING invoice_id")
    Integer saveInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Select("INSERT INTO invoice_detail (invoice_id, product_id)" +
    "VALUES (#{invoiceId}, #{productId})"
    )
    Integer saveProductById(Integer invoiceId, Integer productId);
}
