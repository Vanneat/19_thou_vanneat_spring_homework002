package com.example.spring_homework02.repository;

import com.example.spring_homework02.model.entity.Customer;
import com.example.spring_homework02.model.request.CustomerRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {


        @Select("SELECT * FROM customers")
        List<Customer> findAllCustomer();

        @Select("SELECT * FROM customers WHERE customer_id= #{customerId}")
        Customer getCustomerById(Integer customerId);

        @Delete("DELETE FROM customers WHERE customer_id=#{customerId}")
        boolean deleteCustomerById(Integer customerId);


        @Select("INSERT INTO customers (customer_name,customer_address,customer_phone) VALUES(#{request.customer_name},#{request.customer_address},#{request.customer_phone}) RETURNING customer_id")
        Integer saveCustomer(@Param("request") CustomerRequest customerRequest);


        @Select("UPDATE customers " +
                "SET customer_name = #{request.customer_name}, " +
                "customer_address = #{request.customer_address}, " +
                "customer_phone = #{request.customer_phone} " +
                "WHERE customer_id = #{customerId} "+
                "RETURNING customer_id"
        )
        Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);
}
