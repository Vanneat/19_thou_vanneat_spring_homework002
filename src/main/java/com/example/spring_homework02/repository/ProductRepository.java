package com.example.spring_homework02.repository;

import com.example.spring_homework02.model.entity.Product;
import com.example.spring_homework02.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM products")
   List<Product> findAllProduct();

    @Select("SELECT * FROM products WHERE product_id= #{productId}")
    Product getProductById(Integer productId);
    @Delete("DELETE FROM products WHERE product_id=#{productId}")
    boolean deleteProductById(Integer productId);


    @Select("INSERT INTO products (product_name, product_price) VALUES(#{request.product_name},#{request.product_price}) RETURNING product_id")
    Integer saveProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE products " +
     "SET product_name = #{request.product_name}, " +
     "product_price = #{request.product_price} " +
     "WHERE product_id = #{productId} " +
     "RETURNING product_id"
    )
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);




    @Select("SELECT p.product_id, p.product_name, p.product_price FROM products p inner join invoice_detail i on p.product_id = i.product_id where invoice_id = #{invoiceId}")
  List<Product> getProductByInvoiceId (Integer invoiceId);
}
