package com.example.spring_homework02.service;

import com.example.spring_homework02.model.entity.Invoice;
import com.example.spring_homework02.model.entity.Product;
import com.example.spring_homework02.model.request.InvoiceRequest;


import java.util.List;

public interface InvoiceService {

    List<Invoice> getAllInvoices();
    Invoice getInvoiceById(Integer invoiceId);

    Integer addNewInvioce(InvoiceRequest invoiceRequest);
}
