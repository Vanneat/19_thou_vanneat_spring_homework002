package com.example.spring_homework02.service;

import com.example.spring_homework02.model.entity.Product;
import com.example.spring_homework02.model.request.ProductRequest;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();

    Product getProductById(Integer productId);

    boolean deleteProductById(Integer productId);

    Integer addNewProduct(ProductRequest productRequest);

    Integer updateProduct(ProductRequest productRequest, Integer productId);
}
