package com.example.spring_homework02.service.serviceImp;

import com.example.spring_homework02.model.entity.Invoice;
import com.example.spring_homework02.model.request.InvoiceRequest;
import com.example.spring_homework02.repository.InvoiceRepository;
import com.example.spring_homework02.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoices() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.findById(invoiceId);
    }

    @Override
    public Integer addNewInvioce(InvoiceRequest invoiceRequest) {
        Integer storInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);
        for (Integer productId: invoiceRequest.getProducts()
             ) {
            invoiceRepository.saveProductById(storInvoiceId, productId);

        }

        return storInvoiceId;
    }
}
