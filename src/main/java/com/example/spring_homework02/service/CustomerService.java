package com.example.spring_homework02.service;

import com.example.spring_homework02.model.entity.Customer;
import com.example.spring_homework02.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();

    Customer getCustomerById(Integer customerId);

    boolean deleteCustomerById(Integer customerId);

    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);
}
