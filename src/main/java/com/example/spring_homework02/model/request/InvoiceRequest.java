package com.example.spring_homework02.model.request;

import com.example.spring_homework02.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {
    private Timestamp invoice_date;
    private Integer customer_id;
    private List<Integer> products;
}
