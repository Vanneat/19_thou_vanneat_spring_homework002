package com.example.spring_homework02.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRespone<T> {

        private String message;
        private T payload;
        private HttpStatus httpStatus;
        private Timestamp timestamp;
}
